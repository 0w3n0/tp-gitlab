const request = require('supertest');
const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000; // Utilisation du port 3000 par défaut

// Route pour gérer les requêtes GET vers la racine du serveur
app.get('/', (req, res) => {
  res.send('Hello, World!'); // Réponse "Hello, World!"
});

// Démarrer le serveur et écouter sur le port spécifié
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});